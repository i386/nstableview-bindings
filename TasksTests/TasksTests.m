//
//  TasksTests.m
//  TasksTests
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "TasksTests.h"

@implementation TasksTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    STFail(@"Unit tests are not implemented yet in TasksTests");
}

@end
