//
//  KMAppDelegate.h
//  Tasks
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KMAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (strong, readwrite) NSMutableArray *tasks;
@property (strong) IBOutlet NSArrayController *controller;

@end
