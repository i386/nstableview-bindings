//
//  Task.m
//  Scratch
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMTask.h"

@implementation KMTask

@synthesize description = _description;

-(id)initWithDescription:(NSString *)description
{
    _description = description;
    return [super init];
}

@end
