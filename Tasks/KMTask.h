//
//  Task.h
//  Scratch
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KMTask : NSObject

@property (copy, readwrite) NSString *description;

-(id)initWithDescription:(NSString *)description;

@end
