//
//  KMAppDelegate.m
//  Tasks
//
//  Created by James Dumay on 25/03/12.
//  Copyright (c) 2012 Atlassian. All rights reserved.
//

#import "KMAppDelegate.h"
#import "KMTask.h"

@implementation KMAppDelegate

@synthesize window = _window;
@synthesize tasks = _tasks;
@synthesize controller = _controller;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    _tasks = [[NSMutableArray alloc] init];
    
    [_tasks addObject:[[KMTask alloc] initWithDescription:@"Task 1"]];
    [_tasks addObject:[[KMTask alloc] initWithDescription:@"Task 2"]];
    [_tasks addObject:[[KMTask alloc] initWithDescription:@"Task 3"]];
    [_tasks addObject:[[KMTask alloc] initWithDescription:@"Task 1"]];
    [_tasks addObject:[[KMTask alloc] initWithDescription:@"Task 1"]];
    
    [_controller setContent:_tasks];
}

@end
